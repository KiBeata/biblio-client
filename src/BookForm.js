import React from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';
import Select from 'react-select';
import './App.css';

class BookForm extends React.Component {


  constructor(props) {
    super(props);
    if(this.props.match.params.id) {
      this.title = 'Editare carte';
    } else {
      this.title = 'Adăugare carte';
    }
    this.state = {title: '', apparitionDate: 2020,
                    author: '', genre: -1, msg: '',
                    selectedGenre: [{id:-1, name:''}],
                    selectedAuthor: [{id:-1, name:''}],
                    color:'red', formError: false};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.typeState = {
      disabled: false,
      dropup: false,
      flip: false,
      highlightOnlyResult: false,
      minLength: 0,
      open: undefined,
    };
    this.authors = [];
    this.genres = [];
  }

  componentDidMount() {

    fetch("http://localhost:8000/api/author/list")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({isLoaded:true});
          if(result.authors)
            result.authors.map(item => this.authors.push({id: item.id, name: item.firstname+" "+item.lastname}));
        },
        (error) => {
        this.setState({isLoaded:true, error:error});
        }
      )
      fetch("http://localhost:8000/api/genre/list")
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({isLoaded:true});
            if(result.genres) {
              result.genres.map(item => this.genres.push({id: item.id, name: item.name}));
            }
          },
          (error) => {
            this.setState({isLoaded:true, error:error});
          }
        )
        if(this.props.match.params.id) {
            fetch("http://localhost:8000/api/book/get/"+this.props.match.params.id)
                .then(res => res.json())
                .then(
                  (result) => {
                      this.setState({isLoaded: true});
                      if(result.book) {
                        this.setState({title: result.book.title,
                                        apparitionDate: result.book.aparitionYear,
                                        author: result.book.authorId,
                                        genre: result.book.genreId
                                      });
                        var bookgenres = [];
                        result.bookgenres.map(item => bookgenres.push({id: item.id, name: item.name}));
                        var genre = this.genres.filter(element => element.id === this.state.genre);
                        var author = this.authors.filter(element => element.id === this.state.author);
                        this.setState({selectedGenre:bookgenres, selectedAuthor:author});
                      }
                  },
                  (error) => {
                    this.setState({isLoaded: true, formError: error});
                  }
                )
          }
    }

  handleChange(event) {
    switch (event.target.name) {
      case 'title':
        this.setState({ title: event.target.value});
        break;
      case 'apparitionDate':
        this.setState({ apparitionDate: event.target.value});
        break;
      default:
        break;

    }

  }

  setAuthor(author) {
    if(author[0]!==undefined) {
      this.setState({selectedAuthor:author, author:author[0].id});
    } else {
      this.setState({selectedAuthor:author});
    }
  }

  setGenre(genre) {
    this.setState({selectedGenre:genre});
  }

  isSelected(item) {
    if(this.state.selectedGenre.includes(item)) {
      return 'selected';
    } else
      return ' ';
  }

  handleSubmit(event) {
    event.preventDefault();
    if(this.props.match.params.id) {
      const data = new FormData(event.target);
      for (var i = 0; i < this.state.selectedGenre.length; i++) {
        data.append('genres[]',this.state.selectedGenre[i].id);
      }
     fetch('http://localhost:8000/api/book/update/'+this.props.match.params.id, {
       method: 'post',
        body: data
      }).then(res => res.json())
      .then((result) => {
        this.setState({msg:'Success!', color:'green', formError:false});
      });
    } else {
      const data = new FormData(event.target);
      for (var i = 0; i < this.state.selectedGenre.length; i++) {
        data.append('genres[]',this.state.selectedGenre[i].id);
      }
      fetch('http://localhost:8000/api/book/add', {
        method: 'post',
        body: data
      }).then(res => res.json())
      .then((result) => {
        this.setState({msg:'Success!', color:'green', formError:false});
      });
    }
  }

  render() {
    const msg = this.state.msg;
    const color= this.state.color;
    return (
      <div className="contentwrap">
        <h1>{this.title}</h1>
        <div className="msgspace" style={{ color: color }}>{msg}</div>
        <form method="post" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="title">
            Titlu
          </label>
          <input type="text" name="title" value={this.state.title} onChange={this.handleChange} placeholder="Enter title here ..."/>
        </div>
        <div className="form-group">
          <label htmlFor="apparitionDate">
            Anul apariției
          </label>
          <input type="text" name="apparitionDate" value={this.state.apparitionDate} onChange={this.handleChange} placeholder="Enter apparition date here ..."/>
        </div>
        <div className="form-group">
          <label htmlFor="author">
            Autor
          </label>
          <Typeahead
          {... this.typeState }
              id="biblio-author-search"
              labelKey="name"
              className="mr-sm-2"
              onChange={author => this.setAuthor(author)}
              selected={this.state.selectedAuthor}
              options={this.authors}
              placeholder="Choose author..."
            />
            <input type="hidden" value={this.state.author} name="author"/>
        </div>
        <div className="form-group">
          <label htmlFor="genre">
            Gen
          </label>
          <Typeahead
          {... this.typeState }
          multiple
              id="biblio-genre-search"
              labelKey="name"
              className="mr-sm-2"
              onChange={genre => this.setGenre(genre)}
              selected={this.state.selectedGenre}
              options={this.genres}
              placeholder="Choose genre..."
            />
            <select type="hidden" name="genres[]" multiple={true} id="genres">
                {this.state.selectedGenre.map(
                  item => (
                    <option value={item.id} key={item.id} defaultValue>{item.name}</option>
                  )
                )}
              </select>


        </div>
        <div className="form-group">
          <input type="submit" className="btn btn-info" value="Submit" />
        </div>
        </form>
      </div>
    )
  }
}

export default BookForm;
