import React from 'react';
import {Route, Link, HashRouter } from "react-router-dom";
import './App.css';
import AuthorForm from './AuthorForm';

class Author extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8000/api/author/list")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({isLoaded: true});
          if(result.authors)
            this.setState({ items: result.authors });
        },
        (error) => {
          this.setState({isLoaded: true, error: error});
        }
      )
    }

    handleRemove(itemId) {
      const newList = this.state.items.filter((item) => item.id !== itemId);
      fetch('http://localhost:8000/api/author/delete/' + itemId, {
            method: 'DELETE',
          })
          .then(res => res.json())
          .then((result) => { this.setState({ items: newList }) },
          (error) => {
            this.setState({error:error});
          });
    }

    render() {
      const { error, isLoaded, items } = this.state;
      if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
          return (
            <HashRouter>
            <table className="table">
          <thead className="thead-dark"><tr>
          <th scope="col">Nume</th>
          <th scope="col">Prenume</th>
          <th scope="col">An naștere</th>
          <th scope="col">Acțiune</th>
          </tr></thead><tbody>
            {items.map(item => (
              <tr key={item.id}>
                <th>{item.lastname}</th>
                <td>{item.firstname}</td>
                <td>{new Date(item.birth_date).getFullYear()}</td>
                <td>
                <span>
                  <Link to={'/author/edit/'+item.id}>
                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-pencil-square" fill="blue" xmlns="http://www.w3.org/2000/svg">
                      <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                      <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                    </svg>
                  </Link>
                </span>
                <span onClick={() => this.handleRemove(item.id)}>
                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-file-x-fill" fill="red" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M12 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM6.854 6.146a.5.5 0 1 0-.708.708L7.293 8 6.146 9.146a.5.5 0 1 0 .708.708L8 8.707l1.146 1.147a.5.5 0 0 0 .708-.708L8.707 8l1.147-1.146a.5.5 0 0 0-.708-.708L8 7.293 6.854 6.146z"/>
                  </svg>
                </span>
                </td>
              </tr>

            ))}
            </tbody>
            </table>




            </HashRouter>
          );
      }
    }
  }

export default Author;
