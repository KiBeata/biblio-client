import React from 'react';
import {  Route, NavLink, HashRouter } from "react-router-dom";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Typeahead } from 'react-bootstrap-typeahead';
import Book from './Book';
import BookForm from './BookForm';
import Author from './Author';
import AuthorForm from './AuthorForm';
import Genre from './Genre';
import GenreForm from './GenreForm';


import './App.css';

function App() {
  var state = {
    disabled: false,
    dropup: false,
    flip: false,
    highlightOnlyResult: false,
    minLength: 2,
    open: undefined,
  };
  var options = [
  {id: 1, name: 'John'},
  {id: 2, name: 'Miles'},
  {id: 3, name: 'Charles'},
  {id: 4, name: 'Herbie'},
];
  return (
    <div className="App">
      <header className="">
        <img src="/bookshelv.jpg" className="App-header" alt="header" />
      </header>
      <HashRouter>
      <Navbar bg="light" expand="lg">
  <Navbar.Brand href="#home">Biblioteca Personala</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <NavDropdown title="Author" id="basic-nav-dropdown">
        <NavLink to="/author/list">Author list</NavLink>
        <NavLink to="/author/add">Add author</NavLink>
      </NavDropdown>
      <NavDropdown title="Genre" id="basic-nav-dropdown">
        <NavLink to="/genre/list">Genre list</NavLink>
        <NavLink to="/genre/add">Add genre</NavLink>
      </NavDropdown>
      <NavDropdown title="Book" id="basic-nav-dropdown">
        <NavLink to="/book/list">Book list</NavLink>
        <NavLink to="/book/add">Add book</NavLink>
      </NavDropdown>
    </Nav>
  </Navbar.Collapse>
</Navbar>
          <Route path="/book/list" component={Book}/>
          <Route path="/genre/list" component={Genre}/>
          <Route path="/author/list" component={Author}/>
          <Route path="/book/add" component={BookForm}/>
          <Route path="/genre/add" component={GenreForm}/>
          <Route path="/author/add" component={AuthorForm}/>
          <Route path="/author/edit/:id" component={AuthorForm}/>
          <Route path="/genre/edit/:id" component={GenreForm}/>
          <Route path="/book/edit/:id" component={BookForm}/>
</HashRouter>

    </div>
  );
}

export default App;
