import React, {useState, useEffect} from 'react';
import DatePicker from "react-datepicker";
import { format } from "date-fns";
import "react-datepicker/dist/react-datepicker.css";
import './App.css';

class AuthorForm extends React.Component {

  constructor(props) {
    super(props);
    if(this.props.match.params.id){
      this.title="Editare autor";
    } else {
      this.title = "Adăugare autor";
    }
    this.state = {firstname: '', lastname: '',
                  birthdate: null, deathdate: null,
                  hasNotDied: true, msg: '',color:'red',
                  formError: false, isLoaded: false};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  setAuthor(author) {
    console.log(author);
    this.setState({firstname:author.firstname,
                    lastname:author.lastname,
                  birthdate: new Date(author.birth_date)});
    if(author.deathdate===null) {
      this.setState({deathdate:null});
    }else {
      this.setState({hasNotDied:false});
    }
  }

  componentDidMount() {
    if(this.props.match.params.id) {
        fetch("http://localhost:8000/api/author/get/"+this.props.match.params.id)
            .then(res => res.json())
            .then(
              (result) => {

                  this.setState({isLoaded: true});
                  if(result.author[0]) {
                    this.setAuthor(result.author[0]);

                }
              },
              (error) => {
                this.setState({isLoaded: true, formError: error});
              }
            )
      } else {
        this.setState({birthdate:new Date(), hasDied: false});
      }
    }

  componentWillUnmount() {

  }

  handleChange(event) {
    switch (event.target.name) {
      case 'firstname':
        this.setState({ firstname: event.target.value});
        break;
      case 'lastname':
        this.setState({ lastname: event.target.value});
        break;
      case 'birthdate':
        this.setState({ birthdate: event.target.value});
        break;
      case 'deathdate':
        this.setState({ deathdate: event.target.value});
        break;
      case 'deathtoggle':
        if(event.target.checked) {
          this.setState({ hasNotDied: event.target.checked, deathdate: null});
        } else {
          this.setState({ hasNotDied: event.target.checked, deathdate: new Date()});
        }
        break;
      default:
        break;

    }

  }

  handleSubmit(event) {
    event.preventDefault();
    if(event.target[0].value==='' || event.target[1].value==='') {
      this.setState({firstname: event.target[0].value, lastname: event.target[1].value, msg:'Numele este obligatoriu!', color:'red', formError:true});
    }else if(event.target[2].value==='') {
      this.setState({firstname: event.target[0].value, lastname: event.target[1].value, msg:'Data nașterii este obligatoriu!', color:'red', formError:true});
    }else {
      const data = new FormData(event.target);
      if(this.props.match.params.id==null) {
        fetch('http://localhost:8000/api/author/add', {
          method: 'post',
          body: data
        }).then(res => res.json())
        .then((result) => {
          this.setState({msg:'Succes!', color:'green', formError:false});
        });
      } else {
        fetch('http://localhost:8000/api/author/update/'+this.props.match.params.id, {
          method: 'post',
          body: data
        }).then(res => res.json())
        .then((result) => {
          this.setState({msg:'Succes!', color:'green', formError:false});
        });
      }
    }
  }


  render() {
    const msg = this.state.msg;
    const color= this.state.color;
    return (
      <div className="contentwrap">
        <h1>{this.title}</h1>
        <div className="msgspace" style={{ color: color }}>{msg}</div>
        <form method="post" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="firstname">
            Nume <i className="require">*</i>
          </label>
          <input type="text" name="firstname" value={this.state.firstname} onChange={this.handleChange} placeholder="Enter authors first name here ..."/>
        </div>
        <div className="form-group">
          <label htmlFor="lastname">
            Prenume <i className="require">*</i>
          </label>
          <input type="text" name="lastname" value={this.state.lastname} onChange={this.handleChange} placeholder="Enter authors last name here ..."/>
        </div>
        <div className="form-group">
          <label htmlFor="birthdate">
            Data nașterii <i className="require">*</i>
          </label>
          <DatePicker name="birthdate" selected={this.state.birthdate} onChange={date => this.setState({birthdate:date})} />
        </div>
        <div className="form-group">
          <label htmlFor="deathdate">
            Anul decesului
          </label>
          <DatePicker name="deathdate" selected={this.state.deathdate} onChange={date => this.setState({deathdate:date})} />
          <input type="checkbox" name="deathtoggle" checked={this.state.hasNotDied} onChange={this.handleChange}/>
        </div>
          <input type="submit" className="btn btn-info" value="Submit" />
        </form>
      </div>
    )
  }
}

export default AuthorForm;
