import React from 'react';

import './App.css';

class GenreForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {name: '', msg: '',color:'red', formError: false};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    if(this.props.match.params.id) {
        fetch("http://localhost:8000/api/genre/get/"+this.props.match.params.id)
            .then(res => res.json())
            .then(
              (result) => {
                  this.setState({isLoaded: true});
                  if(result.genre[0]) {
                    this.setState({name:result.genre[0].name});
                }
              },
              (error) => {
                this.setState({isLoaded: true, formError: error});
              }
            )
      } else {
        this.setState({name:''});
      }
    }

  handleChange(event) {
    this.setState({ name: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    if(event.target[0].value === '') {
      this.setState({ name: event.target[0].value, msg: 'Numele este obligatoriu!', color:'red', formError:true});
    } else {
      const data = new FormData(event.target);
      if(this.props.match.params.id==null) {
        fetch('http://localhost:8000/api/genre/add', {
          method: 'post',
          body: data
        }).then(res => res.json())
        .then((result) => {
          if(result.newgenre === true) {
              this.setState({msg:'Success!', formError:false, color:'green'});
            }
        });
      } else {
        fetch('http://localhost:8000/api/genre/update/'+this.props.match.params.id, {
          method: 'post',
          body: data
        }).then(res => res.json())
        .then((result) => {
          if(result.updatedgenre === true) {
              this.setState({msg:'Success!', formError:false, color:'green'});
          }
        });
      }
    }
  }

  render() {
    const msg = this.state.msg;
    const color= this.state.color;
    return (
      <div className="contentwrap">
        <h1>Adăugare gen</h1>
        <div className="msgspace" style={{ color: color }}>{msg}</div>
        <form method="post" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="name">
            Nume <i className="require">*</i>
          </label>
          <input type="text" name="name" value={this.state.name} onChange={this.handleChange} placeholder="Enter genre name here ..."/>
        </div>
        <input type="submit" className="btn btn-info" value="Submit" />
        </form>
      </div>
    )
  }
}

export default GenreForm;
